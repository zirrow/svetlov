<?php
// Newsdv Module for Opencart v1.5.5, modified by villagedefrance (contact@villagedefrance.net)

// Heading 
$_['heading_title']   	= 'Last news';

// Text
$_['text_more']  		= 'Read more...';
$_['text_posted'] 		= 'Posted :';

// Buttons
$_['buttonlist']     	= 'View all';
?>
