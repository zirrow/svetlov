<?php echo $header; ?>

<?php if($this->config->get('themer_subcategory_photo') != 'on') {?>
<style type="text/css">
  .category-list ul li img { display: none; }
  .category-list ul li span { background-color: #FAFAFA; }
</style>
<?php } ?>

 <div class="breadcrumb-100">
  <div class="breadcrumb">
      <?php $w_bc_total = count($breadcrumbs); if ($w_bc_total > 0) {
		$w_bc_last = $w_bc_total - 1;
		foreach ($breadcrumbs as $i => $breadcrumb) { ?>
		<?php if ($i == $w_bc_last) { break; } ?>
		<i><span><?php echo $breadcrumb['separator']; ?></span></i><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
       <?php } ?>
       <i><span><?php echo $breadcrumbs[$w_bc_last]['separator']; ?></span></i><?php echo $breadcrumbs[$w_bc_last]['text']; ?><?php } ?>
  </div>
 </div>

<?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content">
<?php echo $content_top; ?>

<div class="category-details">

	<div class="category-details-top">
	    <?php if ($thumb) { ?>
	        <div class="image"><img src="<?php echo $thumb; ?>" alt="<?php echo $heading_title; ?>" /></div>
	    <?php } ?>
	    <div class="category-h1"><h1><?php echo $heading_title; ?></h1></div>
			<?php if ($description) { ?>
				<div class="clear"></div>
				<div class="category-info">
					<?php echo $description; ?>
				</div>
			<?php } ?>
			<?php if ($description_bottom) { ?>
				<div class="clear"></div>
				<div class="category-info-bottom">
					<?php echo $description_bottom; ?>
				</div>
			<?php } ?>
	</div>

  <?php if ($categories) { ?>
	  <p class="refine-p"><?php echo $text_refine; ?></p>

	  <div class="category-list">
	    <?php if (count($categories)) { ?>
	    <ul>
	      <?php foreach ($categories as $category) { ?>
	      <li><a href="<?php echo $category['href']; ?>"><img src="<?php echo $category['thumb']; ?>"><span><?php echo $category['name']; ?></a></span></li>
	      <?php } ?>
	    </ul>
	    <?php } ?>
	  </div>
  <?php } ?>

  <?php if ($products) { ?>
  <div class="product-filter">
    <div class="product-compare">
	<a href="<?php echo $compare; ?>" class="icon-compare"></a>
	<a href="<?php echo $compare; ?>" id="compare-total-2"><?php echo $text_compare; ?></a>
    </div>
    <div class="limit"><b class="limitb"><?php echo $text_limit; ?></b>
      <select onchange="location = this.value;">
        <?php foreach ($limits as $limits) { ?>
        <?php if ($limits['value'] == $limit) { ?>
        <option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $limits['text']; ?></option>
        <?php } else { ?>
        <option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
        <?php } ?>
        <?php } ?>
      </select>
    </div>
    <div class="sort"><b class="sortb"><?php echo $text_sort; ?></b>
      <select onchange="location = this.value;">
        <?php foreach ($sorts as $sorts) { ?>
        <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
        <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
        <?php } else { ?>
        <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
        <?php } ?>
        <?php } ?>
      </select>
    </div>
  </div>

<div class="display"><a class="select"><?php echo $text_list; ?></a> <a class="notselect" onclick="display('grid');"><?php echo $text_grid; ?></a></div>

</div>
<div>
  <div class="product-list">
    <?php foreach ($products as $product) { ?>
    <div>
      <?php if ($product['thumb']) { ?>
      <div class="image"><?php echo $product['sticker']; ?>

      <?php if ($product['price']) { ?>
        <?php if (!$product['special']) { ?>
        	<?php } else { ?>
	<span class="sale">-<?php echo $product['saving']; ?>%</span>
        <?php } ?>
      <?php } ?>

	<a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" title="<?php echo $product['name']; ?>" alt="<?php echo $product['name']; ?>" /></a></div>
      <?php } ?>
      <div class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
      <div class="description"><?php echo $product['description']; ?></div>
      <?php if ($product['price']) { ?>
      <div class="price">
        <?php if (!$product['special']) { ?>
        <?php echo $product['price']; ?>
        <?php } else { ?>
        <span class="price-old"><?php echo $product['price']; ?></span> <span class="price-new"><?php echo $product['special']; ?></span>
        <?php } ?>
        <?php if ($product['tax']) { ?>
        <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
        <?php } ?>
      </div>
      <?php } ?>
	  <div class="rating">
	      <?php if ($product['rating']) { ?>
			<img src="catalog/view/theme/default/image/stars-<?php echo $product['rating']; ?>.png" alt="<?php echo $product['reviews']; ?>" />
			<?php } else { ?>
			<img src="catalog/view/theme/default/image/stars-0.png" alt="<?php echo $product['reviews']; ?>" />
	      <?php } ?>

		  <?php if ($product['special']) { ?>
			  <?php if ($product['special_end'] && $this->config->get('countdowntimer_category')) { ?>
				    <div class="countdown-cat">
						<?php echo $text_countdown; ?>
						<div id="countdown<?php echo $product['product_id']?>"></div>
					</div>
				  <script>
					  setInterval(function(){ countdown("<?php echo $product['special_end'];  ?>", <?php echo $product['product_id']?>); }, 50);
				  </script>
			  <?php } ?>
		  <?php } ?>

	  </div>
      <div class="cart"><a title="<?php echo $button_cart; ?>" onclick="addToCart('<?php echo $product['product_id']; ?>');" class="button" /></a></div>
      <div class="wishlist"><a title="<?php echo $button_wishlist; ?>" onclick="addToWishList('<?php echo $product['product_id']; ?>');"></a></div>
      <div class="compare"><a title="<?php echo $button_compare; ?>" onclick="addToCompare('<?php echo $product['product_id']; ?>');"></a></div>
    </div>
    <?php } ?>
  </div>
  <div class="pagination"><?php echo $pagination; ?></div>
  <?php } ?>

  <?php if (!$categories && !$products) { ?>
  <div class="empty-content"><?php echo $text_empty; ?></div>
  <div class="buttons">
    <div class="right"><a href="<?php echo $continue; ?>" class="button"><i class="fa fa-share"></i><?php echo $button_continue; ?></a></div>
  </div>
  <?php } ?>

</div>
</div>

<script type="text/javascript"><!--
function display(view) {
	if (view == 'list') {
		$('.product-grid').attr('class', 'product-list');
		
		$('.product-list > div').each(function(index, element) {
			html  = '<div class="right">';

			var price = $(element).find('.price').html();
			
			if (price != null) {
				html += '<div class="price">' + price  + '</div>';
			}

			html += '  <div class="list-button"><div class="cart">' + $(element).find('.cart').html() + '</div>';
			html += '  <div class="wishlist">' + $(element).find('.wishlist').html() + '</div>';
			html += '  <div class="compare">' + $(element).find('.compare').html() + '</div></div>';
			html += '</div>';			
			
			html += '<div class="left">';
			
			var image = $(element).find('.image').html();
			
			if (image != null) { 
				html += '<div class="image">' + image + '</div>';
			}
			
					
			html += '  <div class="name">' + $(element).find('.name').html() + '</div>';
			html += '  <div class="description">' + $(element).find('.description').html() + '</div>';
			
			var rating = $(element).find('.rating').html();
			
			if (rating != null) {
				html += '<div class="rating">' + rating + '</div>';
			}
				
			html += '</div>';
						
			$(element).html(html);
		});		
		
		$('.display').html('<a class="select"><?php echo $text_list; ?></a> <a class="notselect" onclick="display(\'grid\');"><?php echo $text_grid; ?></a>');
		
		$.totalStorage('display', 'list'); 
	} else {
		$('.product-list').attr('class', 'product-grid');
		
		$('.product-grid > div').each(function(index, element) {
			html = '';
			
			var image = $(element).find('.image').html();
			
			if (image != null) {
				html += '<div class="image">' + image + '</div>';
			}
			

			html += '<div class="grid-button"><div class="cart">' + $(element).find('.cart').html() + '</div>';
			html += '<div class="wishlist">' + $(element).find('.wishlist').html() + '</div>';
			html += '<div class="compare">' + $(element).find('.compare').html() + '</div></div>';

			html += '<div class="name">' + $(element).find('.name').html() + '</div>';
			html += '<div class="description">' + $(element).find('.description').html() + '</div>';

			var rating = $(element).find('.rating').html();
			
			if (rating != null) {
				html += '<div class="grid-table"><div class="rating">' + rating + '</div>';
			}

			var price = $(element).find('.price').html();
			
			if (price != null) {
				html += '<div class="price">' + price  + '</div></div>';
			}
			
			$(element).html(html);
		});	
					
		$('.display').html('<a class="notselect" onclick="display(\'list\');"><?php echo $text_list; ?></a> <a class="select"><?php echo $text_grid; ?></a>');
		
		$.totalStorage('display', 'grid');
	}
}

view = $.totalStorage('display');

if (view) {
  display(view);
} else {
  display('<?php echo $this->config->get('themer_products_view'); ?>');
}
//--></script> 

<?php if ($this->config->get('themer_category_desc') == 'on' and $page_number == 1) { ?>
	<?php if ($description) { ?>
	  <script type="text/javascript"><!--
	    $(document).ready(function() {
	    $('#content').after('<div style="clear:both;"></div><div id="bottom-description"></div>');

	    $('#bottom-description').append('<div class="category-info-down"></div>');
	    $('.category-info-down').append('<div class="category-details-top"><?php if ($thumb) { ?><span class="image"><img src="<?php echo $thumb; ?>"></span><?php } ?><span class="category-h3"><h3><?php echo $heading_title; ?></h3></span></div>');
	    $('.category-info-down').append($('.category-info-bottom'));

	    });
	    //--></script>
	<?php } ?>

<?php } ?>

<?php if($this->config->get('countdowntimer_category')){ ?>
	<script>
		function countdown(product_date, product_id){
			<?php if($countdowntimer_category_texttimer){ ?>
			var names = {days:      JSON.parse(JSON.stringify(<?php echo $text_countdown_days; ?>)),
	                    hours:     JSON.parse(JSON.stringify(<?php echo $text_countdown_hours; ?>)),
	                    minutes:   JSON.parse(JSON.stringify(<?php echo $text_countdown_minutes; ?>)),
	                    seconds:   JSON.parse(JSON.stringify(<?php echo $text_countdown_seconds; ?>)),
			};

		    var day_name = names['days'][3];
		    var hur_name = names['hours'][3];
		    var min_name = names['minutes'][3];
		    var sec_name = names['seconds'][3];
		    <?php }else{ ?>
			    var day_name = ":";
			    var hur_name = ":";
			    var min_name = ":";
			    var sec_name = "";
		    <?php } ?>


			var today = new Date();

			var BigDay = new Date(product_date);
			var timeLeft = (BigDay.getTime() - today.getTime());

			var e_daysLeft = timeLeft / 86400000;
			<?php if($countdowntimer_category_days || $countdowntimer_category_countdays){ ?>
			var daysLeft = Math.floor(e_daysLeft);
			    <?php if($countdowntimer_category_texttimer){ ?>
				    <?php if($config_language == "en"){ ?>
				    if(parseInt(daysLeft) == 1){
				        day_name = names['days'][1];
				    }else{
				        day_name = names['days'][2];
				    }
				    <?php }else{ ?>
				    var slice_day = String(daysLeft).slice(-1);
				    if(parseInt(slice_day) == 1 && (parseInt(daysLeft) < 10 || parseInt(daysLeft) > 20)){
				        day_name = names['days'][1];
				    }else if((parseInt(slice_day) == 2 || parseInt(slice_day) == 3 || parseInt(slice_day) == 4) && (parseInt(daysLeft) < 10 || parseInt(daysLeft) > 20)){
				        day_name = names['days'][2];
				    }else{
				        day_name = names['days'][3];
				    }
				    <?php } ?>
			    <?php } ?>
			<?php }else{ ?>
			var daysLeft = 0;
			<?php } ?>

			var e_hrsLeft = (e_daysLeft - daysLeft)*24;
			var hrsLeft = Math.floor(e_hrsLeft);
			if(hrsLeft < 10){
				hrsLeft = '0'+hrsLeft;
			}
				<?php if($countdowntimer_category_texttimer){ ?>
					<?php if($config_language == "en"){ ?>
				    if(parseInt(hrsLeft) == 1){
				        hur_name = names['hours'][1];
				    }else{
				        hur_name = names['hours'][2];
				    }
				    <?php }else{ ?>
				    var slice_hours = String(hrsLeft).slice(-1);
				    if(parseInt(slice_hours) == 1 && (parseInt(hrsLeft) < 10 || parseInt(hrsLeft) > 20)){
				        hur_name = names['hours'][1];
				    }else if((parseInt(slice_hours) == 2 || parseInt(slice_hours) == 3 || parseInt(slice_hours) == 4)  && (parseInt(hrsLeft) < 10 || parseInt(hrsLeft) > 20)){
				        hur_name = names['hours'][2];
				    }else{
				        hur_name = names['hours'][3];
				    }
				    <?php } ?>
			    <?php } ?>

			var e_minsLeft = (e_hrsLeft - hrsLeft)*60;
			var minsLeft = Math.floor(e_minsLeft);
			if(minsLeft < 10){
				minsLeft = '0'+minsLeft;
			}
				<?php if($countdowntimer_category_texttimer){ ?>
					<?php if($config_language == "en"){ ?>
				    if(parseInt(minsLeft) == 1){
				        min_name = names['minutes'][1];
				    }else{
				        min_name = names['minutes'][2];
				    }
				    <?php }else{ ?>
				    var slice_min = String(minsLeft).slice(-1);
				    if(parseInt(slice_min) == 1 && (parseInt(minsLeft) < 10 || parseInt(minsLeft) > 20)){
				        min_name = names['minutes'][1];
				    }else if((parseInt(slice_min) == 2 || parseInt(slice_min) == 3 || parseInt(slice_min) == 4) && (parseInt(minsLeft) < 10 || parseInt(minsLeft) > 20)){
				        min_name = names['minutes'][2];
				    }else{
				        min_name = names['minutes'][3];
				    }
				    <?php } ?>
			    <?php } ?>

			var seksLeft = Math.floor((e_minsLeft - minsLeft)*60);
			if(seksLeft < 10){
				seksLeft = '0'+seksLeft;
			}
				<?php if($countdowntimer_category_texttimer){ ?>
					<?php if($config_language == "en"){ ?>
				    if(parseInt(seksLeft) == 1){
				        sec_name = names['seconds'][1];
				    }else{
				        sec_name = names['seconds'][2];
				    }
				    <?php }else{ ?>
				    var slice_sec = String(seksLeft).slice(-1);
				    if(parseInt(slice_sec) == 1 && (parseInt(seksLeft) < 10 || parseInt(seksLeft) > 20)){
				        sec_name = names['seconds'][1];
				    }else if((parseInt(slice_sec) == 2 || parseInt(slice_sec) == 3 || parseInt(slice_sec) == 4) && (parseInt(seksLeft) < 10 || parseInt(seksLeft) > 20)){
				        sec_name = names['seconds'][2];
				    }else{
				        sec_name = names['seconds'][3];
				    }
				    <?php } ?>
			    <?php } ?>
			    <?php if(!$countdowntimer_category_seconds && !$countdowntimer_category_texttimer){ ?>
				    min_name = '';
			    <?php } ?>

			if (BigDay.getTime() > today.getTime() ){
				document.getElementById("countdown"+product_id).innerHTML = <?php if($countdowntimer_category_days){ ?>daysLeft+day_name+<?php } ?>hrsLeft+hur_name+minsLeft+min_name<?php if($countdowntimer_category_seconds){ ?>+seksLeft+sec_name<?php } ?>;
			}
		}
	</script>
<?php } ?>


<?php echo $footer; ?>