<?php
// Heading
$_['heading_title'] = '';
$_['email_request_subject'] = 'Заказать звонок';
$_['text_recall']         = 'Заказать звонок';
$_['user_page']             = 'Страница товара:';
$_['user_name']             = 'Имя:';
$_['user_phone']            = 'Телефон:';
$_['recommend_to_call']     = 'Желаемое время:';
$_['user_comment']          = 'Комментарий:';
$_['text_oneclick']         = 'Купить в 1 клик';
$_['close_window']          = 'Закрыть окно';
$_['text_success']          = 'Ваша заявка успешно отправлена!';
$_['text_name']             = 'Ваше имя:';
$_['text_phone']            = 'Ваш телефон:';
$_['text_email']            = 'Ваш e-mail:';
$_['text_time']             = 'Желаемое время:';
$_['text_comment']          = 'Вопрос:';
$_['text_request']          = 'Перезвоните мне';
$_['text_error']            = 'Ошибка!';

$_['placeholder_phone']     = 'Укажите номер телефона';
$_['placeholder_name']      = 'Впишите Ваше имя';
$_['placeholder_email']     = 'Впишите Ваш e-mail';
$_['placeholder_comment']   = 'Желательно заполнить';

$_['error_email']      		  = 'Вы указали неверный e-mail';
$_['error_user_name']   	  = 'Имя должно быть от 1 до 32 символов!';
$_['error_recommend_to_call'] = 'Желаемое время должно быть от 1 до 32 символов!';
$_['error_user_comment']      = 'Комментарий должен быть от 1 до 128 символов!';
$_['error_user_phone']        = 'Номер телефона должен быть от 8 до 12 символов';
?>