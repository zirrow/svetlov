<?php echo $header; ?>
	<div class="breadcrumb-100">
		<div class="breadcrumb">
			<?php $w_bc_total = count($breadcrumbs); if ($w_bc_total > 0) {
				$w_bc_last = $w_bc_total - 1;
				foreach ($breadcrumbs as $i => $breadcrumb) { ?>
					<?php if ($i == $w_bc_last) { break; } ?>
					<i><span><?php echo $breadcrumb['separator']; ?></span></i><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
				<?php } ?>
				<i><span><?php echo $breadcrumbs[$w_bc_last]['separator']; ?></span></i><?php echo $breadcrumbs[$w_bc_last]['text']; ?><?php } ?>
		</div>
	</div>

<?php echo $column_left; ?><?php echo $column_right; ?>
	<div id="content">
<?php echo $content_top; ?>

	<div class="category-details">
		<div class="category-details-top">
			<div class="category-h1"><h1><?php echo $heading_title; ?></h1></div>
		</div>
		<div class="blog-info">
			<?php if ($images) { ?>
				<div class="left">
					<?php if ($images) { ?>
						<div class="image-additional">
							<?php foreach ($images as $image) { ?>
								<a href="<?php echo $image['popup']; ?>" title="<?php echo $heading_title; ?>" class="colorbox" rel="colorbox"><img src="<?php echo $image['thumb']; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" /></a>
							<?php } ?>
						</div>
					<?php } ?>
				</div>
			<?php } ?>
			<div class="blog-description"><?php echo $description; ?></div>
		</div>

		<div class="blog-right">
			<div class="description">
				<?php if ($review_status) { ?>
					<?php if ($article_review) { ?>
						<div class="review">
							<div>
								<?php if ($rating) { ?>
									<img src="catalog/view/theme/default/image/stars-<?php echo $rating; ?>.png" alt="<?php echo $rating; ?>" />
								<?php } else { ?>
									<img src="catalog/view/theme/default/image/stars-0.png" alt="<?php echo $rating; ?>" />
								<?php } ?>
								<?php echo $reviews; ?>
							</div>
							<div class="pluso" data-background="transparent" data-options="small,round,line,horizontal,nocounter,theme=04" data-services="vkontakte,odnoklassniki,moimir,facebook,twitter,google"></div>
						</div>
					<?php } ?>
				<?php } ?>
			</div>
		</div>

		<?php if ($download_status) { ?>
			<div class="blog-info">
				<?php if($downloads){ ?><br />
					<?php foreach($downloads as $download){ ?>
						<a href="<?php echo $download['href']; ?>" title=""><i class="fa fa-floppy-o"></i><?php echo $download['name']; ?> <?php echo " (". $download['size'] .")";?></a><br>
					<?php } ?><br />
				<?php } ?>
			</div>
		<?php } ?>

		<?php if ($review_status) { ?>
			<?php if ($article_review) { ?>
				<div class="blog-review">
					<div id="review"></div>
					<div id="rev" class="prev"><a class="button"><?php echo $text_write; ?></a></div>
					<div id="rev_add" class="visible">
						<b><?php echo $entry_name; ?></b><br />
						<input type="text" name="name" value="" />
						<br />
						<br />
						<b><?php echo $entry_review; ?></b>
						<textarea name="text" cols="40" rows="8" style="width: 98%;"></textarea>
						<span style="font-size: 11px;"><?php echo $text_note; ?></span><br />
						<br />
						<b><?php echo $entry_rating; ?></b> <span><?php echo $entry_bad; ?></span>&nbsp;
						<input type="radio" name="rating" value="1" />
						&nbsp;
						<input type="radio" name="rating" value="2" />
						&nbsp;
						<input type="radio" name="rating" value="3" />
						&nbsp;
						<input type="radio" name="rating" value="4" />
						&nbsp;
						<input type="radio" name="rating" value="5" />
						&nbsp;<span><?php echo $entry_good; ?></span><br />
						<br />
						<b><?php echo $entry_captcha; ?></b><br />
						<input type="text" name="captcha" value="" />
						<br />
						<img src="index.php?route=blog/article/captcha" alt="" id="captcha" /><br />
						<br />
						<div class="buttons">
							<div class="right"><a id="button-review" class="button"><?php echo $button_continue; ?></a></div>
						</div>
					</div>
				</div>
			<?php } ?>
		<?php } ?>
	</div>

	<?php if ($products) { ?>
		<div class="box">
			<div class="box-heading"><i class="fa fa-fire"></i> <?php echo $tab_related_product; ?> (<?php echo count($products); ?>)</div>
			<div class="box-content">
				<div id="owl-example11" class="owl-carousel owl-theme tab-proddv" style="display: block; opacity: 1;">
					<?php foreach ($products as $product) { ?>
						<div class="item">
							<?php if ($product['thumb']) { ?>
								<div class="image"><?php echo $product['sticker']; ?>

									<?php if ($product['price']) { ?>
										<?php if (!$product['special']) { ?>
										<?php } else { ?>
											<span class="sale">-<?php echo $product['saving']; ?>%</span>
										<?php } ?>
									<?php } ?>

									<a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" /></a>
								</div>
							<?php } ?>
							<div class="info">
								<div class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
								<?php if ($product['price']) { ?>
									<div class="price">
										<?php if (!$product['special']) { ?>
											<?php echo $product['price']; ?>
										<?php } else { ?>
											<span class="price-old"><?php echo $product['price']; ?></span> <span class="price-new"><?php echo $product['special']; ?></span>
										<?php } ?>
									</div>
								<?php } ?>
								<a onclick="addToCart('<?php echo $product['product_id']; ?>');" class="button"><i class="fa fa-shopping-cart"></i> <?php echo $button_cart; ?></a>
								<?php if ($product['rating']) { ?>
									<div class="rating"><img src="catalog/view/theme/default/image/stars-<?php echo $product['rating']; ?>.png" alt="<?php echo $product['reviews']; ?>" /></div>
								<?php } ?>
							</div>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>

		<script>
			$(document).ready(function () {
				if (!$("#owl-example11").parents("#column-left, #column-right").length) {
					$("#owl-example11").owlCarousel({

						// Отображение
						itemsCustom : [[0, 1], [461, 2], [750, 3], [1000, 4]],

						//Скорость прокрутки
						slideSpeed : 500,
						paginationSpeed : 800,
						rewindSpeed : 1000,

						//Автопролистывание
						autoPlay : true,
						stopOnHover : true,

						// Кнопки навигации < >
						navigation : true,
						rewindNav : true,
						scrollPerPage : false,  // сразу в конец или начало

					});
				}
			});
		</script>
	<?php } ?>

	<?php if ($articles) { ?>
	<div class="box">
		<div class="box-heading"><i class="fa fa-file"></i> <?php echo $tab_related; ?> (<?php echo count($articles); ?>)</div>
		<div class="box-content">
			<div id="owl-example112" class="owl-carousel owl-theme tab-proddv" style="display: block; opacity: 1;">
				<?php foreach ($articles as $article) { ?>
					<div class="item">
						<div class="article-left">
							<?php if ($article['thumb']) { ?>
								<div class="image"><a href="<?php echo $article['href']; ?>"><img src="<?php echo $article['thumb']; ?>" alt="<?php echo $article['name']; ?>" /></a></div>
							<?php } ?>
						</div>
						<div class="name"><a href="<?php echo $article['href']; ?>"><?php echo $article['name']; ?></a></div>
						<div class="description-article"><?php echo $article['description']; ?> <a href="<?php echo $article['href']; ?>">...&raquo;</a></div>
						<div class="rating">
							<img src="catalog/view/theme/default/image/stars-<?php echo $article['rating']; ?>.png" alt="<?php echo $article['reviews']; ?>" />
						</div>
					</div>
				<?php } ?>
			</div>
		</div>
	</div>

		<script>
			$(document).ready(function () {
				if (!$("#owl-example112").parents("#column-left, #column-right").length) {
					$("#owl-example112").owlCarousel({

						// Отображение
						itemsCustom : [[0, 1], [461, 2], [750, 3], [1000, 4]],

						//Скорость прокрутки
						slideSpeed : 500,
						paginationSpeed : 800,
						rewindSpeed : 1000,

						//Автопролистывание
						autoPlay : true,
						stopOnHover : true,

						// Кнопки навигации < >
						navigation : true,
						rewindNav : true,
						scrollPerPage : false,  // сразу в конец или начало

					});
				}
			});
		</script>
	<?php } ?>

  <?php echo $content_bottom; ?>
  </div>
  <script type="text/javascript">
$(document).ready(function(){

$('#rev').click(function(){
$('#rev_add').toggleClass('visible');

		});
		
});
</script>
<script type="text/javascript"><!--
$('.colorbox').colorbox({
	overlayClose: true,
	opacity: 0.5
});
//--></script> 
<script type="text/javascript"><!--
$('#button-cart').bind('click', function() {
	$.ajax({
		url: 'index.php?route=checkout/cart/add',
		type: 'post',
		data: $('.blog-info input[type=\'text\'], .blog-info input[type=\'hidden\'], .blog-info input[type=\'radio\']:checked, .blog-info input[type=\'checkbox\']:checked, .blog-info select, .blog-info textarea'),
		dataType: 'json',
		success: function(json) {
			$('.success, .warning, .attention, .information, .error').remove();
			
			if (json['error']) {
				if (json['error']['option']) {
					for (i in json['error']['option']) {
						$('#option-' + i).after('<span class="error">' + json['error']['option'][i] + '</span>');
					}
				}
			} 
			
			if (json['success']) {
				$('#notification').html('<div class="success" style="display: none;">' + json['success'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');
					
				$('.success').fadeIn('slow');
					
				$('#cart-total').html(json['total']);
				
				$('html, body').animate({ scrollTop: 0 }, 'slow'); 
			}	
		}
	});
});
//--></script>
<script type="text/javascript"><!--
$('#review .pagination a').live('click', function() {
	$('#review').fadeOut('slow');
		
	$('#review').load(this.href);
	
	$('#review').fadeIn('slow');
	
	return false;
});			

$('#review').load('index.php?route=blog/article/review&article_id=<?php echo $article_id; ?>');

$('#button-review').bind('click', function() {
	$.ajax({
		url: 'index.php?route=blog/article/write&article_id=<?php echo $article_id; ?>',
		type: 'post',
		dataType: 'json',
		data: 'name=' + encodeURIComponent($('input[name=\'name\']').val()) + '&text=' + encodeURIComponent($('textarea[name=\'text\']').val()) + '&rating=' + encodeURIComponent($('input[name=\'rating\']:checked').val() ? $('input[name=\'rating\']:checked').val() : '') + '&captcha=' + encodeURIComponent($('input[name=\'captcha\']').val()),
		beforeSend: function() {
			$('.success, .warning').remove();
			$('#button-review').attr('disabled', true);
			$('#rev').after('<div class="attention"><img src="catalog/view/theme/default/image/loading.gif" alt="" /> <?php echo $text_wait; ?></div>');
		},
		complete: function() {
			$('#button-review').attr('disabled', false);
			$('.attention').remove();
		},
		success: function(data) {
			if (data['error']) {
				$('#rev').after('<div class="warning">' + data['error'] + '</div>');
			}
			
			if (data['success']) {
				$('#rev').after('<div class="success">' + data['success'] + '</div>');
								
				$('input[name=\'name\']').val('');
				$('textarea[name=\'text\']').val('');
				$('input[name=\'rating\']:checked').attr('checked', '');
				$('input[name=\'captcha\']').val('');
			}
		}
	});
});
//--></script> 
<script type="text/javascript"><!--
$('#tabs a').tabs();
//--></script> 
<script type="text/javascript" src="catalog/view/javascript/jquery/ui/jquery-ui-timepicker-addon.js"></script> 
<script type="text/javascript"><!--
if ($.browser.msie && $.browser.version == 6) {
	$('.date, .datetime, .time').bgIframe();
}

$('.date').datepicker({dateFormat: 'yy-mm-dd'});
$('.datetime').datetimepicker({
	dateFormat: 'yy-mm-dd',
	timeFormat: 'h:m'
});
$('.time').timepicker({timeFormat: 'h:m'});
//--></script>
<script type="text/javascript">(function() {
  if (window.pluso)if (typeof window.pluso.start == "function") return;
  if (window.ifpluso==undefined) { window.ifpluso = 1;
    var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
    s.type = 'text/javascript'; s.charset='UTF-8'; s.async = true;
    s.src = ('https:' == window.location.protocol ? 'https' : 'http')  + '://share.pluso.ru/pluso-like.js';
    var h=d[g]('body')[0];
    h.appendChild(s);
  }})();</script>  
<?php echo $footer; ?>