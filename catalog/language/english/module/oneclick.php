<?php
// Heading
$_['heading_title'] = '';
$_['email_request_subject'] = 'Buy 1 click';
$_['text_recall']           = 'Buy 1 click';
$_['user_page']             = 'product Page:';
$_['user_name']             = 'Name:';
$_['user_phone']            = 'Phone:';
$_['recommend_to_call']     = 'desired time:';
$_['user_comment']          = 'A comment:';
$_['text_oneclick']         = 'Buy 1 click';
$_['close_window']          = 'Close a window';
$_['text_success']          = 'Your application has been successfully sent !';
$_['text_name']             = 'Your name:';
$_['text_phone']            = 'Your phone number:';
$_['text_email']            = 'Your e-mail:';
$_['text_time']             = 'Desired time :';
$_['text_comment']          = 'Question:';
$_['text_request']          = 'call me back';
$_['text_error']            = 'Error!';

$_['placeholder_phone']     = 'Enter phone number';
$_['placeholder_name']      = 'Enter your name';
$_['placeholder_email']     = 'Enter your e-mail';
$_['placeholder_comment']   = 'It is desirable to complete';

$_['error_email']      		  = 'You entered an invalid e-mail';
$_['error_user_name']   	  = 'The name must be from 1 to 32 characters !';
$_['error_recommend_to_call'] = 'The desired time should be 1 to 32 characters !';
$_['error_user_comment']      = 'Comments must be from 1 to 128 characters !';
$_['error_user_phone']        = 'The phone number should be between 8 and 12 characters';
?>