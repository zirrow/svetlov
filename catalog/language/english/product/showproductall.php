<?php
// Text
$_['heading_title']     = 'All Store Products';
$_['heading_title2']    = 'Price list';
$_['heading_title_link']= 'View All Products';
$_['text_refine']       = 'Refine your search';
$_['text_product']      = 'All goods';
$_['text_error']        = 'Product not found!';
$_['text_empty']        = 'No products.';
$_['text_quantity']     = 'Amount:';
$_['text_manufacturer'] = 'Manufacturer:';
$_['text_model']        = 'All products by model:';
$_['text_points']       = 'Reward points:';
$_['text_price']        = 'Price:';
$_['text_tax']          = 'With tax:';
$_['text_reviews']      = 'reviews: %s';
$_['text_compare']      = 'compare (%s)';
$_['text_display']      = 'View:';
$_['text_list']         = 'List';
$_['text_grid']         = 'Grid';
$_['text_sort']         = 'Sort by:';
$_['text_default']      = 'Default';
$_['text_name_asc']     = 'Names (AZ)';
$_['text_name_desc']    = 'Name (Z to A)';
$_['text_price_asc']    = 'Prices (Low &gt; High)';
$_['text_price_desc']   = 'Prices (High &lt; Low)';
$_['text_rating_asc']   = 'Rating (low)';
$_['text_rating_desc']  = 'Rating (High)';
$_['text_model_asc']    = 'Model (A to Z)';
$_['text_model_desc']   = 'Model (Z to A)';
$_['text_limit']        = 'On the page:';
?>