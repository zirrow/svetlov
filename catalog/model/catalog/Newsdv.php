<?php
// Newsdv Module for Opencart v1.5.5, modified by villagedefrance (contact@villagedefrance.net)

class ModelCatalogNewsdv extends Model { 

	public function updateViewed($Newsdv_id) {
		$this->db->query("UPDATE " . DB_PREFIX . "Newsdv SET viewed = (viewed + 1) WHERE Newsdv_id = '" . (int)$Newsdv_id . "'");
	}

	public function getNewsdvStory($Newsdv_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "Newsdv n LEFT JOIN " . DB_PREFIX . "Newsdv_description nd ON (n.Newsdv_id = nd.Newsdv_id) LEFT JOIN " . DB_PREFIX . "Newsdv_to_store n2s ON (n.Newsdv_id = n2s.Newsdv_id) WHERE n.Newsdv_id = '" . (int)$Newsdv_id . "' AND nd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND n2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND n.status = '1'");
	
		return $query->row;
	}

	public function getNewsdv($data) {
				$sql = "SELECT * FROM " . DB_PREFIX . "Newsdv n LEFT JOIN " . DB_PREFIX . "Newsdv_description nd ON (n.Newsdv_id = nd.Newsdv_id) LEFT JOIN " . DB_PREFIX . "Newsdv_to_store n2s ON (n.Newsdv_id = n2s.Newsdv_id) WHERE nd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND n2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND n.status = '1' ORDER BY n.date_added DESC";
	
				if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
				$data['start'] = 0;
				}		
				if ($data['limit'] < 1) {
				$data['limit'] = 10;
				}	
		
				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
				}	
		
				$query = $this->db->query($sql);
	
				return $query->rows;
				}

	public function getNewsdvShorts($limit) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "Newsdv n LEFT JOIN " . DB_PREFIX . "Newsdv_description nd ON (n.Newsdv_id = nd.Newsdv_id) LEFT JOIN " . DB_PREFIX . "Newsdv_to_store n2s ON (n.Newsdv_id = n2s.Newsdv_id) WHERE nd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND n2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND n.status = '1' ORDER BY n.date_added DESC LIMIT " . (int)$limit); 
	
		return $query->rows;
	}

	public function getTotalNewsdv() {
     	$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "Newsdv n LEFT JOIN " . DB_PREFIX . "Newsdv_to_store n2s ON (n.Newsdv_id = n2s.Newsdv_id) WHERE n2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND n.status = '1'");
	
		if ($query->row) {
			return $query->row['total'];
		} else {
			return FALSE;
		}
	}	
}
?>
