<?php echo $header; ?>
	<div class="breadcrumb-100">
		<div class="breadcrumb">
			<?php $w_bc_total = count($breadcrumbs); if ($w_bc_total > 0) {
				$w_bc_last = $w_bc_total - 1;
				foreach ($breadcrumbs as $i => $breadcrumb) { ?>
					<?php if ($i == $w_bc_last) { break; } ?>
					<i><span><?php echo $breadcrumb['separator']; ?></span></i><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
				<?php } ?>
				<i><span><?php echo $breadcrumbs[$w_bc_last]['separator']; ?></span></i><?php echo $breadcrumbs[$w_bc_last]['text']; ?><?php } ?>
		</div>
	</div>
<?php echo $column_left; ?><?php echo $column_right; ?>
	<div id="content">
		<?php echo $content_top; ?>
		<div class="category-details">

			<div class="category-details-top">
				<div class="category-h1"><h1><?php echo $heading_title2; ?></h1></div>

			</div>

			<div class="product-filter">
				<div class="sort"><b class="sortb"><?php echo $text_sort; ?></b>
					<select onchange="location = this.value;">
						<?php foreach ($sorts as $sorts) { ?>
							<?php if ($sorts['value'] == $sort . '-' . $order) { ?>
								<option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
							<?php } else { ?>
								<option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
							<?php } ?>
						<?php } ?>
					</select>
				</div>
			</div>


			<?php if ($products) { ?>


			<div class="product-list-blog">
				<table width="100%">
					<thead>
					<tr>
						<td><?php echo $text_display; ?></td>
						<td><?php echo $text_price; ?></td>
						<td></td>
					</tr>
					</thead>
					<tbody>
					<?php foreach ($products as $product) { ?>
						<tr>
							<td><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></td>
							<?php if ($product['price']) { ?>
							<td>
								<?php if (!$product['special']) { ?>
									<?php echo $product['price']; ?>
								<?php } else { ?>
									<?php echo $product['special']; ?>
								<?php } ?>
							</td>
							<?php } ?>
							<td>
								<? if ((int)$product['price'] > 0) { ?>
									<a title="<?php echo $button_cart; ?>" onclick="addToCart('<?php echo $product['product_id']; ?>');" /><?php echo $button_cart; ?></a>
								<?php } else { ?>
									<a href="<?php echo $product['href']; ?>"><?php echo $button_cart; ?></a>
								<?php } ?>
							</td>
						</tr>
					<?php } ?>
					</tbody>
				</table>
			</div>
			<?php } ?>

		</div>
	</div><?php echo $content_bottom; ?>



<?php echo $footer; ?>