<?php
// Newsdv Module for Opencart v1.5.5, modified by villagedefrance (contact@villagedefrance.net)

class ModelCatalogNewsdv extends Model {

	public function addNewsdv($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "Newsdv SET status = '" . (int)$data['status'] . "', date_added = now()");
	
		$Newsdv_id = $this->db->getLastId();
	
		if (isset($data['image'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "Newsdv SET image = '" . $this->db->escape($data['image']) . "' WHERE Newsdv_id = '" . (int)$Newsdv_id . "'");
		}
	
		if (isset($data['date_added'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "Newsdv SET date_added = '" . $this->db->escape($data['date_added']) . "' WHERE Newsdv_id = '" . (int)$Newsdv_id . "'");
		}
	
		foreach ($data['Newsdv_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "Newsdv_description SET Newsdv_id = '" . (int)$Newsdv_id . "', language_id = '" . (int)$language_id . "', title = '" . $this->db->escape($value['title']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "', description = '" . $this->db->escape($value['description']) . "'");
		}
	
		if (isset($data['Newsdv_store'])) {
			foreach ($data['Newsdv_store'] as $store_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "Newsdv_to_store SET Newsdv_id = '" . (int)$Newsdv_id . "', store_id = '" . (int)$store_id . "'");
			}
		}
		
		if ($data['keyword']) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'Newsdv_id=" . (int)$Newsdv_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
		}
	
		$this->cache->delete('Newsdv');
	}

	public function editNewsdv($Newsdv_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "Newsdv SET status = '" . (int)$data['status'] . "' WHERE Newsdv_id = '" . (int)$Newsdv_id . "'");

		if (isset($data['image'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "Newsdv SET image = '" . $this->db->escape($data['image']) . "' WHERE Newsdv_id = '" . (int)$Newsdv_id . "'");
		}
		
		if (isset($data['date_added'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "Newsdv SET date_added = '" . $this->db->escape($data['date_added']) . "' WHERE Newsdv_id = '" . (int)$Newsdv_id . "'");
		}
		
		$this->db->query("DELETE FROM " . DB_PREFIX . "Newsdv_description WHERE Newsdv_id = '" . (int)$Newsdv_id . "'");
	
		foreach ($data['Newsdv_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "Newsdv_description SET Newsdv_id = '" . (int)$Newsdv_id . "', language_id = '" . (int)$language_id . "', title = '" . $this->db->escape($value['title']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "', description = '" . $this->db->escape($value['description']) . "'");
		}
	
		$this->db->query("DELETE FROM " . DB_PREFIX . "Newsdv_to_store WHERE Newsdv_id = '" . (int)$Newsdv_id . "'");
	
		if (isset($data['Newsdv_store'])) {		
			foreach ($data['Newsdv_store'] as $store_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "Newsdv_to_store SET Newsdv_id = '" . (int)$Newsdv_id . "', store_id = '" . (int)$store_id . "'");
			}
		}
	
		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'Newsdv_id=" . (int)$Newsdv_id . "'");
	
		if ($data['keyword']) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'Newsdv_id=" . (int)$Newsdv_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
		}
	
		$this->cache->delete('Newsdv');
	}

	public function deleteNewsdv($Newsdv_id) { 
		$this->db->query("DELETE FROM " . DB_PREFIX . "Newsdv WHERE Newsdv_id = '" . (int)$Newsdv_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "Newsdv_description WHERE Newsdv_id = '" . (int)$Newsdv_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "Newsdv_to_store WHERE Newsdv_id = '" . (int)$Newsdv_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'Newsdv_id=" . (int)$Newsdv_id . "'");
	
		$this->cache->delete('Newsdv');
	}

	public function getNewsdvStory($Newsdv_id) { 
		$query = $this->db->query("SELECT DISTINCT *, (SELECT keyword FROM " . DB_PREFIX . "url_alias WHERE query = 'Newsdv_id=" . (int)$Newsdv_id . "') AS keyword FROM " . DB_PREFIX . "Newsdv n LEFT JOIN " . DB_PREFIX . "Newsdv_description nd ON (n.Newsdv_id = nd.Newsdv_id) WHERE n.Newsdv_id = '" . (int)$Newsdv_id . "' AND nd.language_id = '" . (int)$this->config->get('config_language_id') . "'");
	
		return $query->row;
	}

	public function getNewsdvDescriptions($Newsdv_id) { 
		$Newsdv_description_data = array();
	
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "Newsdv_description WHERE Newsdv_id = '" . (int)$Newsdv_id . "'");
	
		foreach ($query->rows as $result) {
			$Newsdv_description_data[$result['language_id']] = array(
				'title'            			=> $result['title'],
				'meta_description' 	=> $result['meta_description'],
				'meta_keyword' 	=> $result['meta_keyword'],
				'description'      		=> $result['description']
			);
		}
	
		return $Newsdv_description_data;
	}

	public function getNewsdvStores($Newsdv_id) { 
		$Newsdvpage_store_data = array();
	
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "Newsdv_to_store WHERE Newsdv_id = '" . (int)$Newsdv_id . "'");
		
		foreach ($query->rows as $result) {
			$Newsdvpage_store_data[] = $result['store_id'];
		}
	
		return $Newsdvpage_store_data;
	}

	public function getNewsdv() { 
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "Newsdv n LEFT JOIN " . DB_PREFIX . "Newsdv_description nd ON (n.Newsdv_id = nd.Newsdv_id) WHERE nd.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY n.date_added");

		return $query->rows;
	}

	public function getTotalNewsdv() { 
		$this->checkNewsdv();
	
     	$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "Newsdv");
	
		return $query->row['total'];
	}

	public function checkNewsdv() { 
		$create_Newsdv = "CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "Newsdv` (`Newsdv_id` int(11) NOT NULL auto_increment, `status` int(1) NOT NULL default '0', `image` VARCHAR(255) COLLATE utf8_general_ci default NULL, `image_size` int(1) NOT NULL default '0', `date_added` date default NULL, `viewed` int(5) NOT NULL DEFAULT '0', PRIMARY KEY (`Newsdv_id`)) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci";
		$this->db->query($create_Newsdv);
	
		$create_Newsdv_descriptions = "CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "Newsdv_description` (`Newsdv_id` int(11) NOT NULL default '0', `language_id` int(11) NOT NULL default '0', `title` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL, `meta_description` VARCHAR(255) COLLATE utf8_general_ci NOT NULL, `description` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL, `meta_keyword` varchar(255) COLLATE utf8_general_ci NOT NULL, PRIMARY KEY (`Newsdv_id`,`language_id`)) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci";
		$this->db->query($create_Newsdv_descriptions);
	
		$create_Newsdv_to_store = "CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "Newsdv_to_store` (`Newsdv_id` int(11) NOT NULL, `store_id` int(11) NOT NULL, PRIMARY KEY (`Newsdv_id`, `store_id`)) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci";
		$this->db->query($create_Newsdv_to_store);
	}
}
?>