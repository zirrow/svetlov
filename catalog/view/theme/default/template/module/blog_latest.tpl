<div class="box">
  <div class="box-heading"><?php echo $heading_title; ?></div>
  <div class="box-content">
    <div class="box-product">
      <?php foreach ($articles as $article) { ?>
	  <div style="margin:0 0 20px;width:100%;">
          <?php if ($article['thumb']) { ?>
		  <div class="image"><a href="<?php echo $article['href']; ?>"><img src="<?php echo $article['thumb']; ?>" alt="<?php echo $article['name']; ?>" title="<?php echo $article['name']; ?>" /></a></div>
          <?php } ?>
          <div class="name" style="max-height:30px;overflow:hidden;"><a href="<?php echo $article['href']; ?>"><?php echo $article['name']; ?></a></div>
          <div class="rating">
          <?php if ($article['rating']) { ?>
	          <img src="catalog/view/theme/default/image/stars-<?php echo $article['rating']; ?>.png" alt="<?php echo $article['rating']; ?>" />
          <?php } else { ?>
	          <img src="catalog/view/theme/default/image/stars-0.png" alt="<?php echo $article['rating']; ?>" />
          <?php } ?>
        </div>
      <div class="description"><?php echo $article['description']; ?> <a href="<?php echo $article['href']?>">...&raquo;</a></div>
      </div>
	  <?php } ?>
    </div>
  </div>
</div>
