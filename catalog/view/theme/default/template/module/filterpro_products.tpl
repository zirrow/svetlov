    <?php foreach ($products as $product) { ?>
    <div>
      <?php if ($product['thumb']) { ?>
      <div class="image"><?php echo $product['sticker']; ?>
	    <?php if ($product['price']) { ?>
            <?php if (!$product['special']) { ?>
        	<?php } else { ?>
				<span class="sale">-<?php echo $product['saving']; ?>%</span>
            <?php } ?>
      <?php } ?>
	  <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" title="<?php echo $product['name']; ?>" alt="<?php echo $product['name']; ?>" /></a></div>
      <?php } ?>
      <div class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
      <div class="description"><?php echo $product['description']; ?></div>
      <?php if ($product['price']) { ?>
      <div class="price">
        <?php if (!$product['special']) { ?>
        <?php echo $product['price']; ?>
        <?php } else { ?>
        <span class="price-old"><?php echo $product['price']; ?></span> <span class="price-new"><?php echo $product['special']; ?></span>
        <?php } ?>
        <?php if ($product['tax']) { ?>
        
        <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
        <?php } ?>
      </div>
      <?php } ?>
	  <div class="rating">
      <?php if ($product['rating']) { ?>
		<img src="catalog/view/theme/default/image/stars-<?php echo $product['rating']; ?>.png" alt="<?php echo $product['reviews']; ?>" />
		<?php } else { ?>
		<img src="catalog/view/theme/default/image/stars-0.png" alt="<?php echo $product['reviews']; ?>" />
      <?php } ?>

		   <?php if ($product['special']) { ?>
			  <?php if ($product['special_end'] && $this->config->get('countdowntimer_category')) { ?>
				  <div class="countdown-cat">
					  <?php echo $text_countdown; ?>
					  <div id="countdown<?php echo $product['product_id']?>"></div>
				  </div>
				  <script>
					  setInterval(function(){ countdown("<?php echo $product['special_end'];  ?>", <?php echo $product['product_id']?>); }, 50);
				  </script>
			  <?php } ?>
		  <?php } ?>

	  </div>
      <div class="cart">
        <input type="button" title="<?php echo $button_cart; ?>" value="" onclick="addToCart('<?php echo $product['product_id']; ?>');" class="button" />
      </div>
      <div class="wishlist"><a title="<?php echo $button_wishlist; ?>" onclick="addToWishList('<?php echo $product['product_id']; ?>');"></a></div>
      <div class="compare"><a title="<?php echo $button_compare; ?>" onclick="addToCompare('<?php echo $product['product_id']; ?>');"></a></div>

    </div>
    <?php } ?>

