<?php if ($reviews) { ?>
<?php foreach ($reviews as $review) { ?>
<div class="review-list" class="category-details">
  <div class="author"><b><?php echo $review['author']; ?></b> <?php echo $text_on; ?> <?php echo $review['date_added']; ?></div>
  <div class="rating">
				  <?php if ($review['rating']) { ?>
					  <img src="catalog/view/theme/default/image/stars-<?php echo $review['rating']; ?>.png" alt="<?php echo $review['rating']; ?>" />
				  <?php } else { ?>
					  <img src="catalog/view/theme/default/image/stars-0.png" alt="<?php echo $review['rating']; ?>" />
				  <?php } ?>
              </div>
  <div class="text"><?php echo $review['text']; ?></div>
</div>
<?php } ?>
<div class="pagination"><?php echo $pagination; ?></div>
<?php } else { ?>
<div class="content"><?php echo $text_no_reviews; ?></div>
<?php } ?>
