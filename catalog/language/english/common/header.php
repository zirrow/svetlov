<?php
// Text
$_['text_myaccount']      = 'My account';
$_['text_home']           = 'Home';
$_['text_category']       = 'Category';
$_['text_wishlist']       = 'Wish List (%s)';
$_['text_compare']        = 'Сравнение (%s)';
$_['text_shopping_cart']  = 'Shopping Cart';
$_['text_search']         = 'Search';
$_['text_welcome']        = '<a href="%s" class="enter">login</a> or <a href="%s" class="registr">create an account</a>.';
$_['text_logged']         = '<a href="%s" class="logged_account">%s</a><a href="%s" class="logged_close"></a>\'';
$_['text_account']        = 'My Account';
$_['text_checkout']       = 'Checkout';
$_['text_contact']        = 'Contacts';
$_['text_page']           = 'Page';
$_['text_blog']	   		  = 'Blog';
$_['text_special']        = 'Specials';
$_['text_latest']         = 'Latest';
$_['text_brands']         = 'Brands';
?>