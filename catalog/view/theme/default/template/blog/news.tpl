<?php echo $header; ?>

	<div class="breadcrumb-100">
		<div class="breadcrumb">
			<?php $w_bc_total = count($breadcrumbs); if ($w_bc_total > 0) {
				$w_bc_last = $w_bc_total - 1;
				foreach ($breadcrumbs as $i => $breadcrumb) { ?>
					<?php if ($i == $w_bc_last) { break; } ?>
					<i><span><?php echo $breadcrumb['separator']; ?></span></i><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
				<?php } ?>
				<i><span><?php echo $breadcrumbs[$w_bc_last]['separator']; ?></span></i><?php echo $breadcrumbs[$w_bc_last]['text']; ?><?php } ?>
		</div>
	</div>

<?php echo $column_left; ?><?php echo $column_right; ?>
	<div id="content">
			<?php echo $content_top; ?>
			<div class="category-details">
				<div class="category-details-top">
					<?php if ($thumb) { ?>
						<div class="image"><img src="<?php echo $thumb; ?>" alt="<?php echo $heading_title; ?>" /></div>
					<?php } ?>
					<div class="category-h1"><h1><?php echo $heading_title; ?></h1></div>
				</div>

				<?php if ($description) { ?>
					<div class="clear"></div>
					<div class="category-info">
						<?php echo $description; ?>
					</div>
				<?php } ?>


			  <?php if ($categories) { ?>
				  <p class="refine-p"><?php echo $text_refine; ?></p>

				  <div class="category-list">
					  <?php if (count($categories)) { ?>
						  <ul>
							  <?php foreach ($categories as $news) { ?>
								  <li><a href="<?php echo $news['href']; ?>"><img src="<?php echo $news['thumb']; ?>"><span><?php echo $news['name']; ?></a></span></li>
							  <?php } ?>
						  </ul>
					  <?php } ?>
				  </div>

			  <?php } ?>
			  <?php if ($articles) { ?>

			   <!-- ocshop -->
				  <div class="product-filter">
					  <div class="limit"><b class="limitb"><?php echo $text_limit; ?></b>
						  <select onchange="location = this.value;">
							  <?php foreach ($limits as $limites) { ?>
								  <?php if ($limites['value'] == $limit) { ?>
									  <option value="<?php echo $limites['href']; ?>" selected="selected"><?php echo $limites['text']; ?></option>
								  <?php } else { ?>
									  <option value="<?php echo $limites['href']; ?>"><?php echo $limites['text']; ?></option>
								  <?php } ?>
							  <?php } ?>
						  </select>
					  </div>
					  <div class="sort"><b class="sortb"><?php echo $text_sort_by; ?></b>
						  <select onchange="location = this.value;">
							  <?php foreach ($sorts as $sorts) { ?>
								  <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
									  <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
								  <?php } else { ?>
									  <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
								  <?php } ?>
							  <?php } ?>
						  </select>
					  </div>
					  <div class="display"><a class="select" id="select-blog"><?php echo $text_list; ?></a> <a class="notselect" id="select-blog" onclick="display('grid');"><?php echo $text_grid; ?></a></div>
					  </div>
				</div>

		<div class="product-list-blog">
			<?php foreach ($articles as $article) { ?>
				<div>
					<?php if ($article['thumb']) { ?>
						<div class="image"><a href="<?php echo $article['href']; ?>"><img src="<?php echo $article['thumb']; ?>" title="<?php echo $article['name']; ?>" alt="<?php echo $article['name']; ?>" /></a></div>
					<?php } ?>
					<div class="name"><a href="<?php echo $article['href']; ?>"><?php echo $article['name']; ?></a></div>
					<div class="description"><?php echo $article['description']; ?></div>
					<div class="more"><a class="button button-more" href="<?php echo $article['href']; ?>"><?php echo $button_more; ?></a></div>
					<div class="added-viewed"><i class="fa fa-clock-o"></i> <?php echo $article["date_added"];?>&nbsp;&nbsp;&nbsp;<i class="fa fa-eye"></i> <?php echo $text_views; ?> <?php echo $article["viewed"];?></div>
					<div class="rating">
						<?php if ($article['rating']) { ?>
							<img src="catalog/view/theme/default/image/stars-<?php echo $article['rating']; ?>.png" alt="<?php echo $article['reviews']; ?>" />
						<?php } else { ?>
							<img src="catalog/view/theme/default/image/stars-0.png" alt="<?php echo $article['reviews']; ?>" />
						<?php } ?>
					</div>
				</div>
			<?php } ?>
		</div>
	  <div class="pagination"><?php echo $pagination; ?></div>
	  <?php } ?>
	  <?php if (!$categories && !$articles) { ?>
	  <div class="content"><?php echo $text_empty; ?></div>
	  <div class="buttons">
	    <div class="right"><a href="<?php echo $continue; ?>" class="button"><?php echo $button_continue; ?></a></div>
	  </div></div>
	  <?php } ?>
	  <?php echo $content_bottom; ?>
	</div>
<script type="text/javascript"><!--
	function display(view) {
		if (view == 'list') {
			$('.product-grid').attr('class', 'product-list-blog');

			$('.product-list-blog > div').each(function(index, element) {

				html = '<div class="left">';

				var image = $(element).find('.image').html();

				if (image != null) {
					html += '<div class="image">' + image + '</div>';
				}


				html += '  <div class="name">' + $(element).find('.name').html() + '</div>';
				html += '  <div class="description">' + $(element).find('.description').html() + '</div>';
				html += '  <div class="more">' + $(element).find('.more').html() + '</div>';
				html += '  <div class="added-viewed">' + $(element).find('.added-viewed').html() + '</div>';

				var rating = $(element).find('.rating').html();

				if (rating != null) {
					html += '<div class="rating">' + rating + '</div>';
				}

				html += '</div>';


				$(element).html(html);
			});

			$('.display').html('<a class="select" id="select-blog"><?php echo $text_list; ?></a> <a class="notselect" id="select-blog" onclick="display(\'grid\');"><?php echo $text_grid; ?></a>');

			$.totalStorage('display', 'list');
		} else {
			$('.product-list-blog').attr('class', 'product-grid');

			$('.product-grid > div').each(function(index, element) {
				html = '';

				var image = $(element).find('.image').html();

				if (image != null) {
					html += '<div class="image">' + image + '</div>';
				}

				html += '<div class="name">' + $(element).find('.name').html() + '</div>';
				html += '<div class="description">' + $(element).find('.description').html() + '</div>';
				html += '<div class="more">' + $(element).find('.more').html() + '</div>';
				html += '<div class="added-viewed">' + $(element).find('.added-viewed').html() + '</div>';

				var rating = $(element).find('.rating').html();

				if (rating != null) {
					html += '<div class="grid-table"><div class="rating">' + rating + '</div></div>';
				}

				$(element).html(html);
			});

			$('.display').html('<a class="notselect" id="select-blog" onclick="display(\'list\');"><?php echo $text_list; ?></a> <a class="select" id="select-blog"><?php echo $text_grid; ?></a>');

			$.totalStorage('display', 'grid');
		}
	}

	view = $.totalStorage('display');

	if (view) {
		display(view);
	} else {
		display('<?php echo $this->config->get('themer_products_view'); ?>');
	}
	//--></script>
<?php echo $footer; ?>