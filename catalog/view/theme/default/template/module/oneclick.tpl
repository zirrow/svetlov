        <a id="oneclick_anchor" class="button" onclick="oneclick_show();" href="javascript:void(0);"><i class="fa fa-fast-forward"></i> <?php echo $text_oneclick; ?></a>
        
<div id="oneclick_form" class="ui-draggable" style="border-radius: 10px 10px 10px 10px; display: none;">

    <div class="big_title"><?php echo $text_oneclick?></div>
    <a onclick="oneclick_close();" class="oneclick_close" href="javascript:void(0);"><i class="fa fa-times"></i></a>

	<div id="oneclick_message2" style="display:none;" class="error_block">
		<span id="oneclick_message" class="error_message" style="text-align:center"></span>
	</div>

	<div style="display:none;" id="oneclick_success" class="success_block">
		<span class="success_message" style="text-align:center">
			<?php echo $text_success?>
		</span>
	</div>

	<form id="oneclick_ajax_form" onsubmit="return oneclick_ajax();" method="POST">
		<input type="hidden" value="yes" name="oneclick">
		<table cellspacing="5" class="form_table_oneclick">
			<tbody>
			<?php if ($show_name) { ?>
            <tr><td colspan="3"><div id="user_name_error" class="error_message"></div></td></tr>        
			<tr>
				<td class="td_oneclick_caption"><?php if( $required_name ) {?><span class="required">*</span>&nbsp;<?php } ?><?php echo $user_name?></td>
                <td colspan="2"><input type="text" class="oneclick_input" value="" placeholder="<?php echo $placeholder_name; ?>" id="user_name" name="user_name"></td>
			</tr>
			<?php } ?>
			<?php if ($show_phone) { ?>
            <tr><td colspan="3"><div id="user_phone_error" class="error_message"></div></td></tr>        
			<tr>
				<td class="td_oneclick_caption"><?php if( $required_phone ) {?><span class="required">*</span>&nbsp;<?php } ?><?php echo $user_phone?></td>
				<td colspan="2"><input type="text" class="oneclick_input" value="" placeholder="<?php echo $placeholder_phone; ?>" id="user_phone" name="user_phone"></td>
			</tr>
			<?php } ?>
			<?php if ($show_email) { ?>
            <tr><td colspan="3"><div id="user_email_error" class="error_message"></div></div></td></tr>
			<tr>
				<td class="td_oneclick_caption"><?php if( $required_email ) {?><span class="required">*</span>&nbsp;<?php } ?><?php echo $text_email?></td>
				<td colspan="2"><input type="text" class="oneclick_input" value="" placeholder="<?php echo $placeholder_email; ?>" id="user_email" name="user_email"></td>
			</tr>
			<?php } ?>
			<?php if ($show_time) { ?>
            <tr><td colspan="3"><div id="recommend_to_call_error" class="error_message"></div></td></tr>
			<tr>
				<td class="td_oneclick_caption"><?php if( $required_time ) {?><span class="required">*</span>&nbsp;<?php } ?><?php echo $text_time?></td>
				<td colspan="2"><input type="text" class="oneclick_input" value="" id="recommend_to_call" name="recommend_to_call"></td>
			</tr>
				<?php } ?>
			<?php if ($show_comment) { ?>
            <tr><td colspan="3"><div id="user_comment_error" class="error_message"></div></td></tr>
			<tr>
				<td class="td_oneclick_caption"><?php if( $required_comment ) {?><span class="required">*</span>&nbsp;<?php } ?><?php echo $text_comment?></td>
				<td colspan="2"><textarea class="oneclick_input" rows="5" cols="20"  placeholder="<?php echo $placeholder_comment; ?>" id="user_comment" name="user_comment"></textarea></td>
			</tr>
				<?php } ?>
			<tr>
			<td align="center" colspan="3">
				<img style="display:none;" id="load_oneclick" src="catalog/view/theme/default/image/loading.gif">
                <a class="button" id="oneclick_submit" href="<?php echo HTTP_SERVER; ?>"><?php echo $text_request?></a>
			</td>
		</tr>
		</tbody></table>
	</form>
</div>
<script>
    $(document).ready(function(){
       $("#oneclick_submit").click(function(e){ e.preventDefault(); $("#oneclick_ajax_form").submit();  })
       $(".overlay").live("click",function(){ oneclick_close(); })
       $("input.input_error").live('keypress', function(){ $(this).removeClass('input_error'); $("#"+$(this).prop("id")+"_error").html('').hide(); });
    });
	function oneclick_close(){
		$('#oneclick_form').hide();
                $(".overlay").remove();
		return false;
	}

	function oneclick_show(){
        margin_top = $('#oneclick_form').height()/2;
        margin_left= -$('#oneclick_form').width()/2;
        $("body").append( $("#oneclick_form") ).append('<div class="overlay"></div>');
        $('#oneclick_form').css({'margin-left': margin_left, 'top': margin_top });
		$('#oneclick_form').show();
		$('.overlay').show();

		$('#oneclick_ajax_form').show();
		$('#oneclick_success').hide();

		$('#user_name').val('');
		$('#user_phone').val('');
		$('#recommend_to_call').val('');
		$('#user_comment').val('');
		$('#oneclick_code').val('');
		return false;
	}

	function show_message_oneclick(id_message, message){
		$('#'+id_message+'_error').html(message).show();
		$("#"+id_message).focus();
		$("#"+id_message).addClass('input_error');
		return false;
	}

	function oneclick_ajax(){
		var vars = $('#oneclick_ajax_form').serialize();
		var page = window.location.href;
		$('#load_oneclick').show();
		$('#submit_oneclick').hide();
		$.ajax({
			type: "POST",
			data: 'oneclick=yes&page='+page+'&'+vars,
			url:'index.php?route=module/oneclick/ajax',
			dataType:'json',
			success: function(json){
				$('#load_oneclick').hide();
				$('#submit_oneclick').show();
				$('.oneclick_input').removeClass('input_error');
				$('.error_message').html('').hide();
				switch (json['result']) {
					case 'success':
						$('#oneclick_message2').hide();
						$('#oneclick_ajax_form').hide();
						$('#oneclick_success').show();
					break;
					case 'error':
					    $.each(json['errors'], 
						function(index, value){
							show_message_oneclick(index, value);
						});

					break;
				}
			}
			});
		return false;
	}
</script>
