<?php
// Text
$_['text_refine']       = 'Выберите подкатегорию';
$_['text_product']      = 'Статьи';
$_['text_error']        = 'Категория не найдена!';
$_['text_empty']        = 'В этой категории нет записей.';
$_['text_display']      = 'Вид:';
$_['text_sort']         = 'Сортировка:';
$_['text_limit']        = 'На странице:';
$_['text_sort_date']    = 'дате';
$_['text_list']         = 'Список';
$_['text_grid']         = 'Сетка';
$_['text_sort_by']      = 'Сортировать по:';
$_['text_default']      = 'По умолчанию';
$_['text_name_asc']     = 'Наименование (А -&gt; Я)';
$_['text_name_desc']    = 'Наименование (Я -&gt; А)';
$_['text_date_asc']     = 'Дата (по возрастанию)';
$_['text_date_desc']    = 'Дата (по убыванию)';
$_['text_rating_asc']   = 'Рейтинг (по возрастанию)';
$_['text_rating_desc']  = 'Рейтинг (по убыванию)';
$_['text_viewed_asc']   = 'Просмотры (по возрастанию)';
$_['text_viewed_desc']  = 'Просмотры (по убыванию)';
$_['text_views'] 	    = 'Просмотров:';

$_['button_more']       = 'подробнее';
?>